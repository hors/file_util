import unittest

from file_util import AtomicReplacement


class TestError(Exception):
    pass


class AtomicReplacementTestCase(unittest.TestCase):
    def _read_file(self):
        with open('testfile') as f:
            return f.read()

    def _write_file(self, s):
        with open('testfile', 'w') as f:
            f.write(s)

    def test_atomic_write(self):
        with AtomicReplacement('testfile', 'w') as f:
            f.write('test')

        self.assertEqual(self._read_file(), 'test')

    def test_atomic_write_fail(self):
        WRITE_STRING = 'write'
        self._write_file(WRITE_STRING)
        try:
            with AtomicReplacement('testfile', 'w') as f:
                f.write('test')
                raise TestError()
        except TestError:
            pass
        self.assertEqual(self._read_file(), WRITE_STRING)

    def test_atomic_write_bad_argument(self):
        self.assertRaises(ValueError, AtomicReplacement, 'testfile', 'r')


if __name__ == '__main__':
    unittest.main()
