# class AtomicReplacement

Atomically write data to a target file using 'with' statement.

A file is created with 'with' statement, and when the statement is over,
the target file is replaced with the content that was written inside the
'with' statement.

This is done so that external observers don't see partial (incomplete) data
in target_file.

## Example usage

Assume 'target_file' exists.
```python
with AtomicReplacement('target_file', 'w') as f:
    print('target_file still contains data before the with statement')
    f.write('content')

print('Data inside target_file has been changed.')
```
